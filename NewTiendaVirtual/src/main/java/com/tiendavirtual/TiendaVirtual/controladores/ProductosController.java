/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tiendavirtual.TiendaVirtual.controladores;

import com.tiendavirtual.TiendaVirtual.entity.Producto_db;
import com.tiendavirtual.TiendaVirtual.service.IProductoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author germa
 */
@CrossOrigin(origins = {"http://localhost:4200","http://localhost"})
@RestController
@RequestMapping("/tiendavirtual")

public class ProductosController {
@Autowired
	private IProductoService productoService;    

@GetMapping("/productos/listar")
	public List<Producto_db> index(){
		return productoService.findAll();
	}
	@GetMapping("/productos/buscar/{id}")
	public Producto_db show(@PathVariable Integer id) {
		return productoService.findById(id);
	}
   
@PostMapping("/productos")
	@ResponseStatus(HttpStatus.CREATED)
	public Producto_db create(@RequestBody Producto_db producto) {
		return productoService.save(producto);
	}
	@DeleteMapping("/productos/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id) {
		productoService.delete(id);
	}
@PutMapping("/productos/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public Producto_db update(@RequestBody Producto_db producto,@PathVariable Integer id) {
		Producto_db productoActual = productoService.findById(id);
		productoActual.setNombre_producto(producto.getNombre_producto());
                productoActual.setPrecio_compra(producto.getPrecio_compra());
                productoActual.setCategoria(producto.getCategoria());
                productoActual.setCantidad(producto.getCantidad());
		return productoService.save(productoActual);
	}        

      
}
