/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tiendavirtual.TiendaVirtual.controladores;

import com.tiendavirtual.TiendaVirtual.entity.Usuario;
import com.tiendavirtual.TiendaVirtual.service.IUsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = {"http://localhost:4200","http://localhost"})
@RestController
@RequestMapping("/tiendavirtual")
/**
 *
 * @author germa
 */
public class UsuarioController {

    @Autowired
	private IUsuarioService usuarioService;    

	@GetMapping("/usuarios/{id_usuario}")
	public Usuario show(@PathVariable Integer id_usuario) {
		return usuarioService.findById(id_usuario);
	}
   
    @PostMapping("/usuarios")
	@ResponseStatus(HttpStatus.CREATED)
	public Usuario create(@RequestBody Usuario usuario) {
		return usuarioService.save(usuario);
	}
	@DeleteMapping("/usuarios/{id_usuario}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer id_usuario) {
		usuarioService.delete(id_usuario);
	}
@PutMapping("/usuarios/{id_usuario}")
	@ResponseStatus(HttpStatus.CREATED)
	public Usuario update(@RequestBody Usuario usuario,@PathVariable Integer id_usuario) {
		Usuario usuarioActual = usuarioService.findById(id_usuario);
		usuarioActual.setNombre(usuario.getNombre());
                usuarioActual.setApellido(usuario.getApellido());
                usuarioActual.setCorreo(usuario.getCorreo());
                usuarioActual.setContrasena(usuario.getContrasena());
		return usuarioService.save(usuarioActual);
	}        

      
}
