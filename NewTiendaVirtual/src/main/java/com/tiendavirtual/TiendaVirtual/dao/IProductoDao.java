/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.tiendavirtual.TiendaVirtual.dao;

import com.tiendavirtual.TiendaVirtual.entity.Producto_db;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author germa
 */
public interface IProductoDao extends CrudRepository<Producto_db, Integer>{
   
}
