/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tiendavirtual.TiendaVirtual.dao;

import com.tiendavirtual.TiendaVirtual.entity.Usuario;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author germa
 */
public interface IUsuarioDao extends CrudRepository<Usuario, Integer> {

}    