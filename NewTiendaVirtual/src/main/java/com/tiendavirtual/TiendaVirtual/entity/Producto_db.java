/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tiendavirtual.TiendaVirtual.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author germa
 */
@Entity
@Table(name="producto_bd")

public class Producto_db implements Serializable {
    private static final long serialVersionUID = 1L;
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
    //Atributos      
    private int id_producto;
    @Column(length = 60,nullable = false)
    private String nombre_producto;
    private double precio_compra;
    @Column(length = 60,nullable = false)
    private String categoria;
    private int cantidad;    

    //Constructor

    public Producto_db(String nombre_producto, double precio_compra, String categoria, int cantidad) {
        this.nombre_producto = nombre_producto;
        this.precio_compra = precio_compra;
        this.categoria = categoria;
        this.cantidad = cantidad;
    }

    public Producto_db() {
    }
        
    //Get  and Set

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public double getPrecio_compra() {
        return precio_compra;
    }

    public void setPrecio_compra(double precio_compra) {
        this.precio_compra = precio_compra;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
        
    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }
}
