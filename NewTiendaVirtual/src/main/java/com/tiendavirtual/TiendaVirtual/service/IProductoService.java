/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.tiendavirtual.TiendaVirtual.service;

import com.tiendavirtual.TiendaVirtual.entity.Producto_db;
import java.util.List;

/**
 *
 * @author germa
 */
public interface IProductoService {

    public List<Producto_db> findAll();

    public Producto_db findById(Integer id);

    public Producto_db save(Producto_db producto_db);

    public void delete(Integer id);
}
