/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.tiendavirtual.TiendaVirtual.service;

import com.tiendavirtual.TiendaVirtual.entity.Usuario;
import java.util.List;

/**
 *
 * @author germa
 */
public interface IUsuarioService {

public List<Usuario> findAll();

    public Usuario findById(Integer id_usuario);

    public Usuario save(Usuario usuario);

    public void delete(Integer id_usuario);    
}
