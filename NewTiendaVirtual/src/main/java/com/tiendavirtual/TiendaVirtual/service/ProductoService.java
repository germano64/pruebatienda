/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tiendavirtual.TiendaVirtual.service;

import com.tiendavirtual.TiendaVirtual.dao.IProductoDao;
import com.tiendavirtual.TiendaVirtual.entity.Producto_db;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author germa
 */
@Service
public class ProductoService implements IProductoService {
    //Crud
    @Autowired
	private IProductoDao productoDao;
    
    @Override
    public List<Producto_db> findAll() {
        return (List<Producto_db>) productoDao.findAll();
    }

    @Override
    public Producto_db findById(Integer id) {
        return productoDao.findById(id).orElse(null);
    }

    @Override
    public Producto_db save(Producto_db producto_db) {
        return productoDao.save(producto_db);
    }

    @Override
    public void delete(Integer id) {
        productoDao.deleteById(id);
    }
    
}
