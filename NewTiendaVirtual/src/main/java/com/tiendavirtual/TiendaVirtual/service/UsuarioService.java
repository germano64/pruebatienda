/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.tiendavirtual.TiendaVirtual.service;

import com.tiendavirtual.TiendaVirtual.dao.IUsuarioDao;
import com.tiendavirtual.TiendaVirtual.entity.Usuario;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author germa
 */
@Service
public class UsuarioService implements IUsuarioService {

    //Crud
    @Autowired
    private IUsuarioDao usuarioDao;
    @Override
    public List<Usuario> findAll() {
        return (List<Usuario>) usuarioDao.findAll();
    }
    @Override
    public Usuario findById(Integer id_usuario) {
        return usuarioDao.findById(id_usuario).orElse(null);
    }
    @Override
    public Usuario save(Usuario usuario) {
        return usuarioDao.save(usuario);
    }
    @Override
    public void delete(Integer id_usuario) {
        usuarioDao.deleteById(id_usuario);
    }

}
